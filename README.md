## Par Ryan Djoher
## Djoher-dev.com

## Ryan@djoher-dev.com

## Etape 1 :
Il faut lancer firefox 

## Etape 2 :
Mettre ce lien dans la barre d'adresse : about:preferences#privacy
Vous devriez être dans les options de firefox
Descendez jusqu'à Permissions et cliquez sur "parametres" de "lecture automatique".
Choississez "Autorisez l'audio et la vidéo" dans le menu déroulant

## Etape 3
Installer le module code injector : https://addons.mozilla.org/fr/firefox/addon/codeinjector/

## Etape 3 : 
Dans chaque page internet de la console (exemple : https://www.micromania.fr/playstation-5-105642.html), injectez le code correpondant à la boutique et s'assurer qu'il tourne bien.
Pour ouvrir le fichier ps5.js, je vous conseille le programme Brackets
Pour en être sûr, normalement dans la console du navigateur, trois secondes après le rafraichissement de la page, vous devriez voir le message "pas de ps5". 
Le code est fait pour rafraichir la page toutes les 5 minutes et si une ps5 est dispo, une musique jeux vidéo retro va se lancer.

## ATTENTION 
Si vous ne voulez pas de fuite de mémoire et que votre Firefox plante au bout de quelques heures, ne laissez pas les consoles des onglets ouverts.

